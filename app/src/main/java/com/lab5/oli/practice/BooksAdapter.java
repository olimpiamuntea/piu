package com.lab5.oli.practice;

import android.content.Context;
import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Oli on 11/26/2016.
 */

public class BooksAdapter extends BaseAdapter
{
    public static List<Book> books;
    private Context context;

    public BooksAdapter(Context context)
    {
        this.context = context;
    }

    @Override
    public int getCount() {
        return books.size();
    }

    @Override
    public Book getItem(int position) {
        return books.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public View getView(int position, View convertView, ViewGroup parent)
    {
        // get a reference to the LayoutInflater service
        LayoutInflater inflater =
                (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        // check if we can reuse a previously defined cell which now is not visible anymore
        View myRow = (convertView == null) ?
                inflater.inflate(R.layout.books_list_context, parent, false) : convertView;
        // get the visual elements and update them with the information from the model
        Book b = (Book)getItem(position);
        TextView tva =(TextView)myRow.findViewById(R.id.textViewAuthor);
        TextView tvt=(TextView)myRow.findViewById(R.id.textViewTitle);
        TextView tvd =(TextView)myRow.findViewById(R.id.textViewDescription);
        ImageView im =(ImageView) myRow.findViewById(R.id.imageView);

        tva.setText(b.getAuthor());
        tvt.setText(b.getTitle());
        tvd.setText(b.getDescription());

        int id = context.getResources().getIdentifier("book" + b.getIdPhoto(),"drawable", context.getPackageName());
        im.setImageResource(id);
        return myRow;
    }

}

