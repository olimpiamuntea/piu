package com.lab5.oli.practice;

/**
 * Created by Oli on 11/26/2016.
 */

public class Book {
    private int idPhoto;
    private String author;
    private String title;

    public boolean isFav() {
        return fav;
    }

    public void setFav(boolean fav) {
        this.fav = fav;
    }

    private boolean fav;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public int getIdPhoto() {
        return idPhoto;
    }

    public void setIdPhoto(int idPhoto) {
        this.idPhoto = idPhoto;
    }

    private String description;

    public Book(int idPhoto, String author, String title, String description,boolean fav)
    {
        this.idPhoto=idPhoto;
        this.author =author;
        this.title =title;
        this.description = description;
        this.fav = fav;
    }
}
