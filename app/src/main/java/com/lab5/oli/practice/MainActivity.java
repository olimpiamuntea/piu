package com.lab5.oli.practice;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    TextView userText,passText, successText;
    EditText user,pass;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
    public	void SignInClick(View view)
    {
        user = (EditText) findViewById(R.id.user);
        pass = (EditText) findViewById(R.id.password);
        userText= (TextView) findViewById(R.id.userText);
        passText= (TextView) findViewById(R.id.passText);
        successText = (TextView) findViewById(R.id.successText);
        int a = 2,b = 2;
        if (user.getText().toString().equals(""))
        {
            userText.setText("Empty username");
            userText.setVisibility(View.VISIBLE);
            a = 1;
        }
        else if(user.getText().length()<3)
            {
                userText.setText("Username too short");
                userText.setVisibility(View.VISIBLE);
                a = 1;
            }
            else if(user.getText().toString().equals("oli"))
                {
                    userText.setVisibility(View.INVISIBLE);
                    a = 0;
                }

        if (pass.getText().toString().equals(""))
        {
            passText.setText("Empty pass");
            passText.setVisibility(View.VISIBLE);
            b = 1;
        }
            else if(pass.getText().length()<3)
            {
                passText.setText("Pass too short");
                passText.setVisibility(View.VISIBLE);
                b = 1;
            }
                else if(pass.getText().toString().equals("oli"))
                {
                    passText.setVisibility(View.INVISIBLE);
                    b = 0;
                }

        if(a==2|| b==2)
        {
            successText.setTextColor(Color.RED);
            successText.setText("Username or pass incorect ! ");
            successText.setVisibility(View.VISIBLE);
            userText.setVisibility(View.INVISIBLE);
            passText.setVisibility(View.INVISIBLE);

        }
        else if(a==0 && b==0)
        {
            successText.setTextColor(Color.GREEN);
            successText.setText("You are logged in");
            successText.setVisibility(View.VISIBLE);
            Intent intent = new Intent(this, BooksActivity.class);
            startActivity(intent);
        }
    }
}
