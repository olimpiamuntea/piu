package com.lab5.oli.practice;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.LinkedList;
import java.util.List;

public class BooksActivity extends AppCompatActivity {
    BooksAdapter ba;
    ListView lv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_books);

        ba = new BooksAdapter(this);
        lv = (ListView)findViewById(R.id.listView);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent intent = new Intent(BooksActivity.this, OneBookActivity.class);
                intent.putExtra("book", position);
                startActivity(intent);

            }
        });

        reset();
        registerForContextMenu(lv);

    }
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo)
    {
        super.onCreateContextMenu(menu, v, menuInfo);
        AdapterView.AdapterContextMenuInfo info =
                (AdapterView.AdapterContextMenuInfo)menuInfo;
        menu.setHeaderTitle(((TextView) info.targetView.findViewById(R.id.textViewTitle)).getText());
        getMenuInflater().inflate(R.menu.menu, menu);
    }
    public boolean onContextItemSelected(MenuItem item)
    {
        AdapterView.AdapterContextMenuInfo info =
                (AdapterView.AdapterContextMenuInfo)item.getMenuInfo();
        int id = info.position;
        if(item.getItemId() == R.id.add)
        {
            Book b1 = new Book( 4, "No title", "no author", "No desription",false);
            ba.books.add(b1);
            ba.notifyDataSetChanged();
        }
        else if(item.getItemId() == R.id.remove)
        {
            ba.books.remove(id);
            ba.notifyDataSetChanged();
        }
        return super.onContextItemSelected(item);
    }
    public	boolean	onOptionsItemSelected(MenuItem	item)
    {
        int	id	=	item.getItemId();
        if	(id	==	R.id.resetList)
        {
            reset();
            ba.notifyDataSetChanged();
            return	true;
        }
        else
        if(id	==	R.id.logout)
        {
            signOut();
            return	true;
        }	else
        if(id == R.id.clearFav)
        {

        }
        return	super.onOptionsItemSelected(item);
    }
    public void reset()
    {
        List<Book> list = new LinkedList<Book>();
        Book b1 = new Book(1, "Amintiri din copilarie", "Ion Creanga", "povestiri",false);
        Book b2 = new Book(2, "Poezii", "Mihai Eminescu", "poezii influenta Schopenhauer",true);
        Book b3 = new Book(3, "Mara", "Ioan Slavici", "roman interbelic",true);
        list.add(b1);
        list.add(b2);
        list.add(b3);
        ba.books=list;
        lv.setAdapter(ba);
    }
    public	boolean	onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.menu_bar,	menu);
        return	true;
    }

    private void signOut() {
        AlertDialog.Builder myDialog = new AlertDialog.Builder(this);
        myDialog.setTitle("Logout");
        myDialog.setMessage("Do you really want to log out?");
        myDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                swapMainActivity();
            }
        });
        myDialog.setNegativeButton("No",new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
            }
        });

        AlertDialog alertDialog = myDialog.create();

        alertDialog.show();
    }
    private void swapMainActivity() {
        Intent myIntent = new Intent(this, MainActivity.class);
        this.startActivity(myIntent);
    }
}
