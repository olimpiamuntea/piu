package com.lab5.oli.practice;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

public class OneBookActivity extends AppCompatActivity {

    private Menu menu;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.books_list_context);
        int bookId = getIntent().getExtras().getInt("book");

        Book b = BooksAdapter.books.get(bookId);

        TextView tva =(TextView)findViewById(R.id.textViewAuthor);
        TextView tvt=(TextView)findViewById(R.id.textViewTitle);
        TextView tvd =(TextView)findViewById(R.id.textViewDescription);
        ImageView im =(ImageView) findViewById(R.id.imageView);


        tvt.setText(b.getTitle());
        tva.setText(b.getAuthor());
        tvd.setText(b.getDescription());
        int id = getResources().getIdentifier("book" + b.getIdPhoto(),"mipmap", getPackageName());
        im.setImageResource(id);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        int bookId = getIntent().getExtras().getInt("book");
        Book b = BooksAdapter.books.get(bookId);
        if(b.isFav())
            getMenuInflater().inflate(R.menu.menusinglebook1, menu);
        else
            getMenuInflater().inflate(R.menu.menusinglebook2, menu);
        this.menu = menu;
        return true;
    }
    public	boolean	onOptionsItemSelected(MenuItem item)
    {
        int	id	=	item.getItemId();
        if	(id	==	R.id.removeFromFav)
        {
            getMenuInflater().inflate(R.menu.menusinglebook1, menu);
            return	true;
        }
        else
        if(id	==	R.id.addToFav)
        {
            getMenuInflater().inflate(R.menu.menusinglebook2, menu);
            return	true;
        }
        return	super.onOptionsItemSelected(item);
    }
}
